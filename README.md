# 30 Ray Radial


This project is the reconstruction of some 30-ray radial data. It is preprocessed with the script in data - `process_data.m` to PCA coil
acquisition, select time-frames, z-slice etc.

It is reconstructed with the 4 cartesian methods compared to regular NUFFT.

### Note on packages

This notebook only works the dependent packages are on the path. In fact, when you call `run` one of the first things that happens is it checks that the needed packages are available at the right major version numbers.

You need to download those packages, put them on the path, and make sure the package name matches exactly with `+` plus sign in front to tell MATLAB it's a package.

### +Shared

For this notebook that means you need the +Shared package which is in the same project folder on bitbucket as this notebook is.
