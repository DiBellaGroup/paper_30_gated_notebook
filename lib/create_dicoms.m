function create_dicoms(sliceNumber, dataDate, resultsDate)
  % Function gives important information such as location of template Dicom
  % then attaches DICOM headers to the source data.

  % Inputs help find data relative to dated path.
  % dataDate = '2015-12-09';
  % resultsDate = '2015-12-31';


  % Set variables that never change
  Matlab.path = ['results/' resultsDate];
  Dicom.inputPath = '''/v/raid1a/gadluru/MRIdata/Cardiac/Verio/P030612/ReconData/CV_Radial7Off_golden_newSatprot_5.9ml_gated(154000)/'''

  % Iterate through the 4 methods and each slice
  interpMethodName = {'grid3', 'grog', 'nn', 'nufft'};
  seriesNumber = sliceNumber * 10;

  for methodName = interpMethodName
    % Iteration variables
    name = methodName{1};
    seriesNumber = seriesNumber + 1;

    % Create structs for Shared method
    Matlab.filename = ['30_ray_slice_' sliceNumber '_' name '.mat'];
    Matlab.varName = [name 'Image'];
    Dicom.outputPath = [name '_dicoms/slice_' sliceNumber];
    Dicom.description = [name '-Slice' sliceNumber];

    otherArgs = [' -o series_number_increment ' num2str(seriesNumber)];

    Shared.create_dicoms(Matlab, Dicom, otherArgs)
  end
end
