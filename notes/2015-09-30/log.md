# Profiling

### FFT
From the profiling results in file43 we learn that ifft2c gets called 1704 times and fft2c gets called 1680 times. The 24 time discrepancy is due to the create_image_input for 8 coils 3 times.

That means on average ifft2c costing 169.108 s across 1704 calls is 0.0992 s per call and fft2c was 100.121/1680 calls which is 0.0596 s per call.

Looking deeper it seems that ifft2 is slower than fft2 by a proportional amount.

And the important number to take home is

| ifft2c | fft2c     | avg |
| :------------- | :------------- | :--- |
| 0.0992 s per call       |  0.0596 s per call       | 0.07942 s per call|



### NUFFT

From the NUFFT results in file49 we see that though mtimes gets called 1128 times, the inner function gets called internally 31584 times, or 28 times.

To get an equivalent measure then, I will divide the times by 28 in order to get the time it takes to perform a full nufft or nufft_adj

| nufft_adj | nufft     |
| :------------- | :------------- |
| 0.2141       | 0.1029       |
2.158 times slower | 1.727 times slower |

OK, so how do I reconcile this with the factor of three I was getting before? Hmm

### Speed Factor?

My first thought is to look at file35. It gives the following

|name | calls | total time     | time per call |
| :--- | :------------- | :------------- | :--- |
| maskFFT.mtimes       |    3360    | 283.288 | 0.0843
| multiNUFFT.mtimes | 1120 | 189.130 | 0.1688

Hmm, that's a factor of 2 again.

The other results were obtained via tic/toc around the lines fftObj * and fftObj' *

So it seems reasonable that those are subject to tic-toc errors but the profiler isn't.

To check this I've stashed the profile results I just referenced in 'profile_results/performance' and am running a new trial using the 'cpu' option.
I get the impression that the performance option actually is like tic toc so I'm still not sure what makes the old results what they are.


Nope, using the cpu option does nothing!

Next I'll just have to see if I can get those old result back, I'll just be inserting tic and toc around the functions

 . . .  Time passes . . .

 So after quite a bit more looking into this it seems that the answer is that my "3" is erroneous.

 I had uses the average of forward and backward nufft against the forward fft instead of against the average of forward and backward.

 I thought I had tested the idea that forward and backward fft were the same, but apparently I didn't or I didn't do it rigorously.

 Everything is consistent with this explanation.

 I think the reason we are getting a 2 instead of a 3 as expected is that I am using looped fft2s whereas fessler uses a custom "fast_fftn" function that wraps fftn. It's hard to tell from the profiler if it is actually faster than my method because the number of calls seems to indicate that it's getting called multiple times just like mtimes which means it's doing some internal jumping around. Probably to shape the data in powers of 2 in size?
