% Clear everything out
clear all
close all

% Add all files local to notebook and packages
addpath('..')
addpath('lib')
addpath('~/packages')

% Verify that needed packages are available
verify

% pick the date folder to get data from
dataDate = '2015-12-09';

% Pick which slices you want to reconstruct
slicesRange = [1 5];

% create the folder for results as needed
todaysDate = Shared.todays_date;
mkdir('results', todaysDate)

% Go through desired slices and process
for iSlice = slicesRange
  sliceString = num2str(iSlice);

  % Load data
  file = ['data/' dataDate '/30ray_gated_slice_' sliceString '.mat'];
  load(file)

  % Reconstruct and Reorient
  Results = Shared.reconstruct_data(kSpaceSlice, trajectory, iSlice);
  Results.grid3Image = fliplr(Results.grid3Image);
  Results.grogImage = fliplr(Results.grogImage);
  Results.nnImage = fliplr(Results.nnImage);
  Results.nufftImage = fliplr(Results.nufftImage);

  % Scale to what region of interest?
  roiRows = 140:182;
  roiCols = 118:163;
  Results = Shared.scale_results(Results, roiRows, roiCols, '30_ray');

  % Write raw data
  resultsPath = ['results/' todaysDate '/30ray_slice' sliceString '_results.mat'];
  save(resultsPath, 'Results')
  % Shared.plot_results(Results, sliceString);

  % Write DICOMS
  create_dicoms(sliceString, dataDate, todaysDate);
end
